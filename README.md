- **Configuration**

  - Installation

    - Debian

      - Installer

      ```bash
      sudo apt install git-all
      ```

      - Version

      ```bash
      git --version
      ```

    - Windows

  - Configuration

    - Identité

    ```bash
    git config --global user.name "Jal"
    git config --global user.email "jalil.mestaoui@gmail.com"
    ```

    - Editeur de texte

    ```bash
    git config --global core.editor emacs
    ```

    - Branche par defaut

    ```bash
    git config --global init.defaultBranch main
    ```

    - Verifier configuration

    ```bash
    git config --list
    ```
    
    - Cloner depot distant
  
        - L 'exemple du TD n'existe plus
        - Cloner un autre depot
  
  ```bash
  git clone https://github.com/juliendehos/hydra-check.git
  
  ```
  
  ![](image/1.png)



- **Depot local**

  - Creer dossier+ ajouter ,Modifier des fichiers + initialiser git +status

    ```bash
    mkdir exoGit
    cd exoGit
    touch index.html
    touch style.css
    git init
    git status
    ```
    
    ![](image/2.png)
    
    ![](image/3.png)
  
    ​	![](image/4.png)
    
  - Ajouter+renemmer+supprimer des fichiers
  
  - Ajouter app.java et script.java
  
  ![](image/5.png)
  
  - Rennomer app.java en main.java et supprimer script.java
  
    ![](image/6.png)
  
    
  
- Historique des commits +revenir dans un commit precedent
  
  - Historique des commits 
  
  ![](image/8.png)
  
  - revenir dans un commit precedent (on recupere script.java et app.java)
  
    on recupere bien nos fichier app.java et script.java
    
    ![](image/9.png)
  
- Tag
  
  ![](image/10.png)
  
- Suppression des commit





- **Depot distant**

  - Creation depot Git dans gitlab + recuperation  en locale

    ![](image/11.png)

    ![](image/12.png)

  - Ajout +commit + push de qq fichiers

    ![](image/13.png)

    ​	   	![](image/14.png)

​							![](image/15.png)

​                    

- 

  - Creation d'un autre compte gitlab, l'inviter et recuperer le projet grace à ce compte et le mettre dans le dossier collaborateur

    ![](image/17.png)

    

    ​		

  - push d'un collaborateur (ajout  de fichier page1.html) on voit bien que le collaborateur a pu ajouter un fichier page1.html dans le projet (pusher avec le compte collaborateur)

    ![](/home/jal/Desktop/tutogit/image/18.png)

    ![](/home/jal/Desktop/tutogit/image/19.png)

    ![](image/20.png)

  - modification de fichier script en meme temps par les deux collaborateur on constate bien un probleme de conflit au moment de pusher le projet ,git detecte le conflit et nous informe de le regler

    ![](image/21.png)

    ![](image/22.png)

    ![](image/23.png)

    Rsolution : avec l outil git pull on detecte le conflit et on le modifie manuellement !

    ![](image/24.png)

    ![](image/26.png)

    ![](image/27.png)

    ![](image/28.png)

  

- **Branche**
  
  - colange depot
  
    ![](image/16.png)
  
  - branche b1 + qq commit + fusionner dans main
  
    ![](image/29.png)
  
    ![](image/30.png)
  
  - envoi b1 sur le serveur
  
    ![](image/31.png)
  
    ![](image/32.png)
  
  - b2 + qq commit + fusionner dans main
  
    ![](image/33.png)
  
    ![](image/33.1.png)
  
    ![](image/33.2.png)
  
  - fusionner main dans b1
  
    ![](image/34.png)
  
    ![](image/35.png)
  
  - envoi b2 sur serveur + supprimer sur depot locale + supprimersur depot distant
  
    ![](image/36.png)
  
    ![](image/37.png)
  
    ![](image/38.png)
  
- **Fork** 

  - forker + pull request : le collaborateur fork le projet ,donc  c est presque l 'equivalent de la creation d un nouveaux projet sur son compte gitlab , apres il va faire des pull request pour modifier le projet original (cette methode est notamment utilisée pour la collaboration dans des projet public). l'administrateur du projet original va faire  une verification , soit il merge les modification soit il renvoi ces remarque au collaborateur pour modifier son code et refaire un nouvelle merge request!

    ![](image/39.png)

  - ![](image/40.png)

  

  

 

- ​	
  - branche public + fusionner avec le main + push dans projet public et privé

![](image/45.png)



![](image/46.png)







![](image/48.png)



- ​	

  - nouveux commit dans main et pusher fusionner avec public et pusher tous , on constate bien un conflit qu on doit resoudre avant de pusher 	

    ![](/home/jal/Desktop/tutogit/image/49.png)

![](/home/jal/Desktop/tutogit/image/50.png)

![](/home/jal/Desktop/tutogit/image/51.png)



